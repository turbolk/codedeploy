#!/bin/bash

# Copy the production environment file from S3 to the local installation
#aws s3 cp s3://XXX/env/production.env /var/www/turbolk/.env

sudo mkdir /var/www/turbolk

#sync the 2 folders
rsync -avu --delete --exclude 'vendor' "/opt/codedeploy-agent/deployment-root/$DEPLOYMENT_GROUP_ID/$DEPLOYMENT_ID/deployment-archive/" "/var/www/turbolk"
#cp -rf /opt/codedeploy-agent/deployment-root/$DEPLOYMENT_GROUP_ID/$DEPLOYMENT_ID/deployment-archive/* /var/www/turbolk

ls -lha /var/www/turbolk

# Setup the various file and folder permissions for Laravel
sudo chown -R :www-data /var/www/turbolk
sudo chmod -R 775 /var/www/turbolk/storage/

#find /var/www/turbolk -type d -exec chmod 755 {} +
#find /var/www/turbolk -type f -exec chmod 644 {} +
#chgrp -R www-data /var/www/turbolk/storage /var/www/turbolk/bootstrap/cache
#chmod -R ug+rwx /var/www/turbolk/storage /var/www/turbolk/bootstrap/cache