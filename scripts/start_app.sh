#!/bin/bash

cd /var/www/turbolk/

#sudo php artisan key:generate
sudo php artisan config:clear
sudo php artisan config:cache
sudo php artisan optimize
sudo php artisan route:cache
sudo php artisan view:cache
sudo php artisan event:cache
sudo php artisan cache:table

php artisan vendor:publish --provider="PragmaRX\Google2FALaravel\ServiceProvider"
php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider"
php artisan vendor:publish --provider="Laravel\Scout\ScoutServiceProvider"

sudo php artisan package:discover

if [ -f /var/www/turbolk/storage/framework/down ]; 
then
	php /var/www/turbolk/artisan up
fi
