#!/bin/bash

#check if already down
if [ -f /var/www/turbolk/storage/framework/down ]; 
then
	echo "app already down"
	exit
fi

if [ -f /var/www/turbolk/artisan ]; 
then
	php /var/www/turbolk/artisan down
fi