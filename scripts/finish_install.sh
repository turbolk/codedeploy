#!/bin/bash

# Copy the production environment file from S3 to the local installation
#aws s3 cp s3://XXX/env/production.env /var/www/turbolk/.env

#export HOME="/home/ubuntu";

cd /var/www/turbolk/
sudo composer clear-cache -d /var/www/turbolk/

env=`cat /home/turbolk/env`
echo $env

#if [ -f /var/www/turbolk/.env ]; 
#then
	#remove old env file
	#sudo rm /var/www/turbolk/.env
#fi

sudo aws s3 cp "s3://turboweb1/$env/laravel/.env" /var/www/turbolk

if [ "$env" = "prd" ];
then
    sudo composer install --no-dev --optimize-autoloader -d /var/www/turbolk/
else
	sudo composer install --optimize-autoloader -d /var/www/turbolk/
fi

ls -lha

# Reload PHP-FPM so that any cached code is subsequently refreshed
#service php7.0-fpm reload